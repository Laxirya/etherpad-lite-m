# Etherpad Lite M
## Run Etherpad Lite M
Change into the directory containing Etherpad Lite M.
Run bin/run.sh and open http://localhost:9001 in your browser.


## MySQL configuration
1. Connect to mysql by opening a command prompt and typing the following:
```
mysql -u root -p
```
2. Once logged in, issue the following command to create the database: create database `etherpad-lite`;
3. Grant permissions to a new database account. Replace <user> and <password> with your own values.
```
grant CREATE,ALTER,SELECT,INSERT,UPDATE,DELETE on `etherpad-lite`.* to '<user>'@'localhost' identified by '<password>';
```
4. Leave the mysql client exit
5. Edit settings.json in your Etherpad Lite root folder and change the database settings (if you have a non-default port configured for MySQL you will have to add the "port" setting).
6. Run etherpad lite
7. Exit etherpad-lite, connect again to mysql and run:

```
ALTER DATABASE `etherpad-lite` CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
USE `etherpad-lite`;
ALTER TABLE `store` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
```

# Example of MySQL Socket Config
"dbType" : "mysql",
"dbSettings" : {
    "user"    : "user",
    "port"    : "/var/run/mysqld/mysqld.sock",
    "password": "secret",
    "database": "database"
}
